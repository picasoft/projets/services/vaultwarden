# Vaultwarden

## Mettre à jour

Il suffit de changer le tag de l'image dans le fichier `docker-compose.yml`

## Ajouter un utilisateur

Le propriétaire de l'organisation Picasoft peut inviter de nouveaux membres qui recevront un email de confirmation pour créer leur compte.

## Ce qui serait cool

Le LDAP n'est pas encore directement géré par vaultwarden, cependant on peut synchroniser le LDAP et les comptes de vaultwarden automatiquement grâce à [ce petit service](https://github.com/dani-garcia/vaultwarden/wiki/Syncing-users-from-LDAP), les utilisateurs du LDAP sont alors automatiquement invité à l'instance.
